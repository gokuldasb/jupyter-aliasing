Aliasing - An attempt at simple description
===========================================

This is an illustrated notebook that explains the phenomenon of aliasing.
Being a very igragious design problem, aliasing is something every engineer and
DIYer should know. [Open the notebook](aliasing.ipynb) to know more.

Copyright (c) 2017 Gokul Das B  
Refer [LICENSE.txt](LICENSE.txt) for details on usage and sharing.

Want to discuss this and more? Head over to [MathLovers](https://matrix.to/#/#mathlovers:diasp.in) 
and share your ideas and questions with like-minded maths enthusiasts.
